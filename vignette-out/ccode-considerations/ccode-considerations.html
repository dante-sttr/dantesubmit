<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">

<head>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">


  <link rel="stylesheet" href="C:/Users/jbrin/Documents/R/win-library/4.0/danteSubmit/rmarkdown/templates/dante_dataset/resources/style.css" />

  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->

<meta charset="utf-8" />
<meta name="generator" content="pandoc" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<meta name="title" content="Country Coding Considerations for Dataset Harmonization and Applied Uses" />
<meta id="browse-image" content="ccode-considerations-browse.jpg" />
<meta id="subhead-image" content="ccode-considerations-subhead.jpg" />
<meta id="keywords" content="country codes" />

<title id="header-title">Country Coding Considerations for Dataset Harmonization and Applied Uses</title>
  <style>
      code{white-space: pre-wrap;}
      span.smallcaps{font-variant: small-caps;}
      span.underline{text-decoration: underline;}
      div.column{display: inline-block; vertical-align: top; width: 50%;}

  </style>

</head>

<header id="title-block-header">

<div id="author-block">
<span id="author-name">Joshua Brinks</span>,&nbsp;<span id="author-affiliation"><em>ISciences, LLC</em>&nbsp;<sup><a id="author-email" href="mailto:jbrinks@isciences.com"><i class="far fa-envelope" style="font-size:15px; color: blue"></i></a></sup></br>

<p class="date" id="metadata-date">April 13, 2021</p>
</div>

<div id="image-attribution-block">
Photo by <a href="https://unsplash.com/@supergios?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Jonny Gios</a> on <a href="https://unsplash.com/s/photos/jigsaw-puzzle?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
</div>

<div id="highlights-block">
<h3>Summary</h3>
<ul>
<li>Managing country codes is one of the most frustrating tasks in environment-security analysis.</li>
<li>When attempting to harmonize multiple datasets, intricate knowledge various ideologies is required.</li>
<li>This document reviews important considerations for country code harmonization for 1900-2020 using the V-Dem and Correlates of War coding schemes.</li>
</ul>
</div>


<div id="related-links-block">
<div id="related-packages-block">
<h3> Related Packages: </h3>
<ul>
<li><a href="/demcon">demcon</a></li>
<li><a href="https://github.com/vdeminstitute/vdemdata">vdemdata</a></li>
<li><a href="https://cran.r-project.org/web/packages/cshapes/index.html">cshapes</a></li>
</ul>
</div>

<div id="related-datasets-block">
<h3> Related Data Sets: </h3>
<ul>
<li><a href="/datasets/v-dem">Varieties of Democracy</a></li>
<li><a href="https://correlatesofwar.org/">Correlates of War</a></li>
<li><a href="/datasets/v-dem">cshapes</a></li>
</ul>
</div>

</div>

</header>

<body>

<div id="introduction" class="section level2">
<h2>Introduction</h2>
<p>Managing country codes is one of the most difficult tasks in environment-security analysis, and each dataset treats the concept differently. Country code designations may be very strict and only respect internationally recognized sovereign nations starting with their first official day of independence. Other datasets may include separate observations and codes for territories, disputed states, colonial nations, and other quasi-independent states. Coding systems like the Correlates of War<span class="citation"><sup>1</sup></span> or Gleditsch and Ward<span class="citation"><sup>2</sup></span> fall into the former with strict, to the day, international recognition, while refugee and asylum datasets provided by the United Nations<span class="citation"><sup>3</sup></span> falls into the latter. UN migration and refugee datasets include nearly every conceivable territorial designation/disaggregation (e.g. Puerto Rico, Taiwan, Faroe Islands, etc.).</p>
<p>When using an individual dataset, users need a general awareness for the conceptual approach to the country code specification and any potential inconsistencies, however, when attempting to harmonize multiple datasets, more intricate knowledge is required. Merging multiple datasets without first addressing disparate approaches to country coding will lead to inconsistencies, numerous <code>NA</code> values, and compound downstream error in the analysis. It takes time and experience to anticipate country coding issues and know what to be on the lookout for when undergoing project planning. The goal of this vignette is to outline common issues and considerations for harmonizing multiple country-year datasets. This walkthrough uses the Varieties of Democracy (V-Dem)<span class="citation"><sup>4</sup></span> and Correlates of War coding schemes to illustrate recurrent challenges. They are good datasets to work with while learning for several reasons:</p>
<ul>
<li>They represent opposite ends of the country-coding ideological spectrum.
<ul>
<li>Because V-Dem is focused on polities, it is highly disaggregated and observes long standing claims of independence.</li>
<li>Conversely, CoW is very strict and adheres to official and internationally recognized claims of independence.</li>
</ul></li>
<li>Both datasets are consistent and have little to no errors and inconsistencies. This is helpful for learning, but not entirely representative of real world experiences. On top of differences in coding ideologies, many datasets have a slew of errors and inconsistent applications that further complicate pre-processing.</li>
<li>They both have direct programmatic access via R that makes management, exploration, and visualization easier.
<ul>
<li>The <em>vdemdata</em> package.<span class="citation"><sup>5</sup></span></li>
<li>The <em>states</em> package<span class="citation"><sup>6</sup></span> featuring CoW and G&amp;W code access and manipulation.</li>
<li>The <em>cshapes</em> package;<span class="citation"><sup>7,8</sup></span> vectorized historic state boundaries using either the CoW or G&amp;W schemes.</li>
</ul></li>
<li>V-Dem provides reference materials that are extremely helpful for learners.
<ul>
<li>The Country Coding Manual and Historic Name column in the raw data provide a lot of context for the year to year regime changes, and their reasoning for important decisions.</li>
</ul></li>
</ul>
</div>
<div id="v-dem-country-codes" class="section level2">
<h2>V-Dem Country Codes</h2>
<p>The Varieties of Democracy (V-Dem) presents a collection of highly disaggregated indicators (400+) depicting wide-ranging measures of democracy and institutional characteristics dating back to 1789. In contrast to several other political, social, and environmental datasets, V-Dem demonstrates an extreme level of transparency in their methods and copious documentation. This includes a dedicated supplementary manual depicting their country coding approach; V-Dem Country Coding Units v11.1.<span class="citation"><sup>9</sup></span> This tutorial will highlight the most relevant (in my opinion) portions of their approach and country-year coding decisions.</p>
<p>V-Dem defines a country as</p>
<blockquote>
<p>…a political unit enjoying at least some degree of functional and/or formal sovereignty.</p>
</blockquote>
<p>Generally speaking, V-Dem will have a country-year observation for a country if:</p>
<ol style="list-style-type: decimal">
<li>The state made a formal declaration of independence; even if not yet fully recognized by the international community.</li>
<li>In modern times this concerns states like Kosovo, Taiwan, and Western Sahel, in historic times this concerns states such as Colonial Asia and Africa.</li>
<li>If the state in question operates with some degree of autonomy that distinguishes itself, at least in its polities and institutions, from the parent nation.</li>
</ol>
<p>In my limited experience with V-Dem, this conceptual approach is carried out very consistently, but additional pre-processing is required to successfully merge with more strict datasets. Moreover, the Country Coding Units Manual contains information that helps users construct historical time series of nations that have limited independent observations in the dataset. For example, Bangladesh is coded independently starting in 1971, but to construct a longer historic record, the Country Coding Units Manual states that the following combination of observations may be used:</p>
<ol style="list-style-type: decimal">
<li>India (Princely state of British India (1910 – 1947))</li>
<li>Pakistan (1947 - 1971)</li>
<li>Bangladesh (1971 - )</li>
</ol>
<p>The Country Coding Units Manual is a great reference if you need to combine historical democracy indicators for nations with intermittent internationally recognized sovereignty with other historically disaggregated environmental, migration, or conflict data.</p>
<p>The <code>histname</code> variable in the raw V-Dem dataset is also an excellent source of context for the true nature of regime changes in annual polity. While the <code>country_name</code> field is static and typically reflects the modern, common, country name in the V-Dem dataset, the <code>histname</code> reflects official or historic names and potential states of occupation. Take Somalia for example:</p>
<table class="table table-striped table-hover" style="margin-left: auto; margin-right: auto;">
<caption>
V-Dem country name, V-Dem historic designation, Correlates of War country code, and start and end years for Somalia in the raw V-Dem Version 11.1 dataset.
</caption>
<thead>
<tr>
<th style="text-align:left;">
country_name
</th>
<th style="text-align:left;">
histname
</th>
<th style="text-align:right;">
COWcode
</th>
<th style="text-align:right;">
Start
</th>
<th style="text-align:right;">
End
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;">
Somalia
</td>
<td style="text-align:left;">
Somalia under formal Italian control over most of the territory [except British Somaliland]
</td>
<td style="text-align:right;">
520
</td>
<td style="text-align:right;">
1900
</td>
<td style="text-align:right;">
1909
</td>
</tr>
<tr>
<td style="text-align:left;">
Somalia
</td>
<td style="text-align:left;">
Somalia under effective (more or less) Italian control
</td>
<td style="text-align:right;">
520
</td>
<td style="text-align:right;">
1910
</td>
<td style="text-align:right;">
1940
</td>
</tr>
<tr>
<td style="text-align:left;">
Somalia
</td>
<td style="text-align:left;">
Somalia under British occupation
</td>
<td style="text-align:right;">
520
</td>
<td style="text-align:right;">
1941
</td>
<td style="text-align:right;">
1949
</td>
</tr>
<tr>
<td style="text-align:left;">
Somalia
</td>
<td style="text-align:left;">
UN Trust Territory of Somalia under Italian administration
</td>
<td style="text-align:right;">
520
</td>
<td style="text-align:right;">
1950
</td>
<td style="text-align:right;">
1959
</td>
</tr>
<tr>
<td style="text-align:left;">
Somalia
</td>
<td style="text-align:left;">
Somali Republic [unites Somaliland with the Trust Territory of Somalia]
</td>
<td style="text-align:right;">
520
</td>
<td style="text-align:right;">
1960
</td>
<td style="text-align:right;">
1968
</td>
</tr>
<tr>
<td style="text-align:left;">
Somalia
</td>
<td style="text-align:left;">
Somali Democratic Republic
</td>
<td style="text-align:right;">
520
</td>
<td style="text-align:right;">
1969
</td>
<td style="text-align:right;">
1990
</td>
</tr>
<tr>
<td style="text-align:left;">
Somalia
</td>
<td style="text-align:left;">
Somali Democratic Republic [civil war]
</td>
<td style="text-align:right;">
520
</td>
<td style="text-align:right;">
1991
</td>
<td style="text-align:right;">
2003
</td>
</tr>
<tr>
<td style="text-align:left;">
Somalia
</td>
<td style="text-align:left;">
Transitional Federal Government [civil war]
</td>
<td style="text-align:right;">
520
</td>
<td style="text-align:right;">
2004
</td>
<td style="text-align:right;">
2011
</td>
</tr>
<tr>
<td style="text-align:left;">
Somalia
</td>
<td style="text-align:left;">
Federal Republic of Somalia [civil war]
</td>
<td style="text-align:right;">
520
</td>
<td style="text-align:right;">
2012
</td>
<td style="text-align:right;">
2020
</td>
</tr>
</tbody>
</table>
<p>This provides a clear understanding of Somalia’s historical sovereignty and polity for the past 120 years. When combined with the Country Coding Units Manual, the user is able to make more informative choices regarding their research interests. It also serves as excellent reference material for historical nation-states that is much faster than being sucked into a Wikipedia black hole.</p>
<p>Now we will walk through some of the more important country coding considerations for applied cases using Varieties of Democracy and Correlates of War for 1900-2020.</p>
</div>
<div id="the-americas" class="section level2">
<h2>The Americas</h2>
<div id="canada" class="section level3">
<h3>Canada</h3>
<ul>
<li>CoW codes Canada starting in 1920 with recognition of the League of Nations.</li>
<li>V-Dem codes Canada back to 1841.</li>
</ul>
</div>
<div id="the-carribean" class="section level3">
<h3>The Carribean</h3>
<p>Most of the Caribbean nations are coded by CoW starting with their official independence from the colonial parent nations; usually some time between 1962-1975. V-Dem starts most Caribbean countries between 1789-1900.</p>
</div>
<div id="central-and-south-america" class="section level3">
<h3>Central and South America</h3>
<p>The remaining nations of Central and South America are similarly coded by CoW and V-Dem because they mostly achieved their independence prior to 1900.</p>
</div>
</div>
<div id="europe" class="section level2">
<h2>Europe</h2>
<div id="the-balkans" class="section level3">
<h3>The Balkans</h3>
<p>The Balkans are consistently a source of frustration when attempting to merge disparate datasets. The affected states include Serbia/Yugoslavia, Bosnia and Herzegovina, Kosovo, Croatia, North Macedonia, Slovenia, and Montenegro. V-Dem disaggregates each component for as long as possible while CoW uses the official, internationally recognized, aggregated states. Because of this, when merging with most datasets, V-Dem will require some additional processing. At a minimum this may include averaging across multiple states to calculate Yugoslavia or the State Union of Serbia and Montenegro. Given time and resources, users may also consider calculating weighted averages using population data as not to give Kosovo or Slovenia equal weighting with Serbia/Yugoslavia or Montenegro. The V-Dem Country Coding manual is very helpful in reconstructing the complicated historic record of these nation-states.</p>
<ul>
<li>Montenegro
<ul>
<li>V-Dem (1789-1918; 1998-2019)</li>
<li>CoW (2006- )</li>
</ul></li>
<li>Macedonia
<ul>
<li>V-Dem (1991- )</li>
<li>CoW (1993-)</li>
</ul></li>
<li>Croatia
<ul>
<li>V-Dem (1941-1945 Nazi puppet state, 1991-)</li>
<li>CoW (1992-)</li>
</ul></li>
<li>Yugoslavia / Serbia
<ul>
<li>V-Dem (1804-)</li>
<li>CoW (1878-) no observations for 1942-1943. CoW maintains the YUG character code during the State Union of Serbia and Montenegro (2003-2006) and Republic of Serbia (2006-).</li>
</ul></li>
<li>Bosnia and Herzegovina
<ul>
<li>V-Dem (1992-)</li>
<li>CoW (1992-)</li>
</ul></li>
<li>Kosovo
<ul>
<li>V-Dem (1999-)</li>
<li>CoW (2008-)</li>
</ul></li>
<li>Slovenia
<ul>
<li>V-Dem (1989-)</li>
<li>CoW (1992-)</li>
</ul></li>
</ul>
<div class="figure" style="text-align: center">
<img src="ccode-considerations_files/figure-html/unnamed-chunk-22-1.png" alt="Visual timeline of significant secessions from the former Yugoslavian state as internationally recognized and coded by the Correlates of War and cshapes project. The top panel depicts Yugoslavia (highlighted teal) in its 1989 unified state. Highlighted countries in remaining panels illustrate countries that gained sovereignty since the previous date." width="576" />
<p class="caption">
Visual timeline of significant secessions from the former Yugoslavian state as internationally recognized and coded by the Correlates of War and cshapes project. The top panel depicts Yugoslavia (highlighted teal) in its 1989 unified state. Highlighted countries in remaining panels illustrate countries that gained sovereignty since the previous date.
</p>
</div>
</div>
<div id="austria-and-hungary" class="section level3">
<h3>Austria and Hungary</h3>
<p>V-Dem codes Austria and Hungary separately throughout their entire record. Therefore there is no corresponding record for CoW numeric code 300 (Austria-Hungary 1816-1918). The user must create this designation.</p>
</div>
<div id="germany" class="section level3">
<h3>Germany</h3>
<p>Germany consists of 3 separate designations in both datasets.</p>
<ul>
<li>Germany (CoW 255): This is modern day and pre-WWII “unified” Germany.
<ul>
<li>CoW tracks Germany 1816-1945; 1990-</li>
<li>V-Dem tracks Germany 1789-1944; 1991-</li>
</ul></li>
<li>West Germany (CoW 260): Post-WWII West Germany or the German Federal Republic.
<ul>
<li>CoW tracks West Germany 1955-1990</li>
<li>V-Dem tracks West Germany 1949-1990</li>
</ul></li>
<li>East Germany (CoW 265): Post-WWII East Germany or the German Democratic Republic.
<ul>
<li>CoW tracks East Germany 1954-1990</li>
<li>V-Dem tracks East Germany 1945-1990 (including 1945-1948 Third Reich Occupied by Russia)</li>
</ul></li>
</ul>
<p>Lastly, as a result of these various configurations, CoW does not track any form of Germany during 1946-1953, and V-Dem does not contain “Unified” or West German observations from 1945-1948.</p>
</div>
<div id="czechoslovakia-czech-republic-and-slovak" class="section level3">
<h3>Czechoslovakia, Czech Republic, and Slovak</h3>
<p>CoW includes designations for Czechoslovakia (315; 1918-1992), Czech Republic (316; 1993-), and Slovakia (317; 1993-), whereas V-Dem observes Czechoslovakia and the Czech Republic on a single code (157; 1918-) with Slovakia (201) diverging from 1939-1945 and again permanently in 1993.</p>
<ul>
<li>CoW has no Czechoslovakia observations under German Occupation (1940-1944)</li>
<li>V-Dem has Czechoslovakia observations from 1939-1944 with a historical designation for German occupation.</li>
<li>V-Dem includes Slovak observations for 1939-1944, when they seceded and essentially behaved as a Nazi puppet state.</li>
</ul>
</div>
<div id="former-soviet-union" class="section level3">
<h3>Former Soviet Union</h3>
<p>Former members of the Soviet Union are handled similarly between both datasets. Due to the centralized control over polity and institutions on part of the USSR during this time, V-Dem does not code members independently (compared to colonial Africa or Caribbean). The exceptions are nations with a historical presence prior to annexation/occupation (Lithuania, Latvia, Uzbekistan, etc.). In these cases states are recorded independently from their pre WWI or WWII independence, and then again following the dissolution of the USSR; 1990 (V-Dem) or 1991 (CoW).</p>
</div>
<div id="remaining-notables" class="section level3">
<h3>Remaining Notables</h3>
<p>The remaining notable European countries are Poland, Ireland, and Luxembourg.</p>
<ul>
<li>Poland
<ul>
<li>CoW records Poland from 1918-1939 and 1945-</li>
<li>V-Dem tracks Poland from 1789-1938 and 1944-</li>
<li>Both include gaps for WWII occupation</li>
</ul></li>
<li>Ireland
<ul>
<li>V-Dem includes starting with their declaration 1919-</li>
<li>CoW starts with official independence 1922-</li>
</ul></li>
<li>Luxembourg
<ul>
<li>V-Dem 1815-</li>
<li>CoW 1920-</li>
</ul></li>
</ul>
</div>
</div>
<div id="middle-east-and-north-africa" class="section level2">
<h2>Middle East and North Africa</h2>
<div id="palestine" class="section level3">
<h3>Palestine</h3>
<p>Along with the various configurations of Yugoslavia, Palestine is another consistent source of frustration when carrying out pre-processing. Many datasets and coding schemes do not recognize Palestine. In fact, CoW does not include a code for Palestine. This complicates interdisciplinary research, because including Palestine in your analysis can severely limit the number of additional datasets you can include without introducing more error through imputation or other fixes. V-Dem includes 3 separate designations for Palestine:</p>
<ul>
<li>Palestinian designations in V-Dem
<ul>
<li>Palestine/British Mandate (209): 1918-1948</li>
<li>Palestine/Gaza (138): 1948-1967 and 2007-. This is present day Gaza controlled by Hamas, and not influenced by Israel</li>
<li>Palestine/West Bank (128): 1948-1950 and 1967-. Starting with 2007, this refers to West Bank only (Gaza is coded separately when Hamas gains control in 2007).</li>
</ul></li>
</ul>
</div>
<div id="yemens" class="section level3">
<h3>Yemen(s)</h3>
<p>CoW includes separate codes for the Yemen Arab Republic/North Yemen (678; 1926-1990), unified modern Yemen (679; 1990-), and Yemen People’s Democratic Republic/South Yemen (680; 1967-).</p>
<p>Similar to the Czech Republic, V-Dem tracks historic, North, and modern Yemen on a single code (14; 1789-1850 and 1918-). They provide South Yemen a separate code (23; 1900-1990). V-Dem’s greater historic record for South Yemen includes only the city of Aden and its immediate surroundings from 1900-1963. There is no CoW equivalent for South Yemen/Aden during this time.</p>
<div class="figure" style="text-align: center">
<img src="ccode-considerations_files/figure-html/unnamed-chunk-23-1.png" alt="Visual timeline of internationally recognized sovereign borders of Yemen, North Yemen, and South Yemen. Historic boundaries are determined by the cshapes packages. Note that cshapes does not include land area that lacks international independence. Hence, in the 1946 (left) panel, British Aden (South Yemen) and Djibouti are not present until they've gained their respective independence." width="720" />
<p class="caption">
Visual timeline of internationally recognized sovereign borders of Yemen, North Yemen, and South Yemen. Historic boundaries are determined by the cshapes packages. Note that cshapes does not include land area that lacks international independence. Hence, in the 1946 (left) panel, British Aden (South Yemen) and Djibouti are not present until they’ve gained their respective independence.
</p>
</div>
</div>
</div>
<div id="non-mena-africa" class="section level2">
<h2>Non-MENA Africa</h2>
<p>There are not many complicated distinctions throughout Africa. V-Dem codes most African nations independently starting in 1900 while indicating their colonial protectorate. Depending on the colonial parent nation, most African countries acquired internationally recognized independence between 1955-1975; this is when the CoW record begins. Some notable African countries:</p>
<ul>
<li>Somaliland
<ul>
<li>Recognized by V-Dem as an independent state from 1900-1960 and again starting in 1991-.</li>
<li>CoW (and several other datasets) does not include Somaliland.</li>
</ul></li>
<li>Ethiopia
<ul>
<li>Coded continuously by V-Dem starting in 1789; including Italian occupation from 1936-1941.</li>
<li>Coded intermittently by CoW (1898-1936; 1941-); excludes Italian occupation.</li>
</ul></li>
<li>Sudan and South Sudan
<ul>
<li>They are coded identically by V-Dem and CoW; a separate code for South Sudan starting in 2011.</li>
</ul></li>
</ul>
</div>
<div id="asia" class="section level2">
<h2>Asia</h2>
<p>V-Dem includes a greater historical record for most Asian countries, because, similar to Africa and the Caribbean, there was less centralized control exercised over their polities and institutions during colonial rule. As a result, most Asian states are recorded in V-Dem beginning sometime 1789-1900, and in CoW during one of the waves of colonial (1945-1950) or regional (1971-1975) independence. However, there are a few nation-states with more complicated records in V-Dem and CoW:</p>
<div id="koreas" class="section level3">
<h3>Korea(s)</h3>
<p>CoW tracks Korea on 3 separate numeric codes: Korea (Unified; 730), North Korea (731), and South Korea (732). Conversely, V-Dem assigns a single code for historic/unified Korea and South Korea (42) while placing North Korea on its own code (41). CoW does not list Korea while under Soviet, USA, or Japanese occupation</p>
<ul>
<li>Korea (Unified)
<ul>
<li>CoW (730): 1887-1905</li>
<li>V-Dem (42): Listed as South Korea 1789-</li>
</ul></li>
<li>North Korea
<ul>
<li>CoW (731): 1948-</li>
<li>V-Dem (42): 1945-</li>
</ul></li>
<li>South Korea
<ul>
<li>CoW (732): 1949-</li>
<li>V-Dem (42): Shared code with Unified Korea 1789-1944; South Korea only 1945-</li>
</ul></li>
</ul>
</div>
<div id="vietnam" class="section level3">
<h3>Vietnam</h3>
<p>Similar to Korea(s), V-Dem places historic, unified, Vietnam and post WWII South Vietnam on a single code. North Vietnam is recognized in 1945 and then absorbs the former South Vietnam in 1976 carrying on to the present. CoW does not recognize colonial or occupied Vietnam(s).</p>
<ul>
<li>Historic Vietnam
<ul>
<li>V-Dem (35): 1802-1975; Includes only South Vietnam from 1945-1975.</li>
<li>CoW: Not recognized as a unified state.</li>
</ul></li>
<li>North Vietnam
<ul>
<li>V-Dem (34): 1945-; Includes former South Vietnam from 1976-.</li>
<li>CoW (816): 1954-</li>
</ul></li>
<li>South Vietnam
<ul>
<li>V-Dem (35): 1802-1975; Includes only South Vietnam from 1945-1975.</li>
<li>CoW (817): 1954-</li>
</ul></li>
</ul>
</div>
</div>
<div id="references" class="section level2 unnumbered">
<h2 class="unnumbered">References</h2>
<div id="refs" class="references csl-bib-body" line-spacing="2">
<div id="ref-CorrelatesofWarProject2017" class="csl-entry">
<div class="csl-left-margin">1. </div><div class="csl-right-inline">Correlates of War Project. <em>State <span>System Membership</span> (V2016)</em>. <a href="http://correlatesofwar.org">http://correlatesofwar.org</a> (2017).</div>
</div>
<div id="ref-Gleditsch1999" class="csl-entry">
<div class="csl-left-margin">2. </div><div class="csl-right-inline">Gleditsch, K. S. &amp; Ward, M. D. A revised list of independent states since the congress of <span>Vienna</span>. <em>International Interactions</em> <strong>25</strong>, 393–413 (1999).</div>
</div>
<div id="ref-UnitedNations2017" class="csl-entry">
<div class="csl-left-margin">3. </div><div class="csl-right-inline">United Nations. <em>Trends in <span>International Migrant Stock</span>: <span>The</span> 2017 <span>Revision</span></em>. (2017).</div>
</div>
<div id="ref-Coppedge2020" class="csl-entry">
<div class="csl-left-margin">4. </div><div class="csl-right-inline">Coppedge, M. <em>et al.</em> <em>V-<span>Dem Codebook V10</span></em>. <a href="https://papers.ssrn.com/abstract=3557877">https://papers.ssrn.com/abstract=3557877</a> (2020) doi:<a href="https://doi.org/10.2139/ssrn.3557877">10.2139/ssrn.3557877</a>.</div>
</div>
<div id="ref-V-DemInstitute2020" class="csl-entry">
<div class="csl-left-margin">5. </div><div class="csl-right-inline">V-Dem Institute. <em>Vdemdata</em>. (<span>V-Dem Institute</span>, 2020).</div>
</div>
<div id="ref-Beger2020" class="csl-entry">
<div class="csl-left-margin">6. </div><div class="csl-right-inline">Beger, A. <em>States</em>. (2020).</div>
</div>
<div id="ref-Weidmann2010" class="csl-entry">
<div class="csl-left-margin">7. </div><div class="csl-right-inline">Weidmann, N. B., Kuse, D. &amp; Gleditsch, K. S. The <span>Geography</span> of the <span>International System</span>: <span>The CShapes Dataset</span>. <em>International Interactions</em> <strong>36</strong>, 86–106 (2010).</div>
</div>
<div id="ref-Weidmann2016" class="csl-entry">
<div class="csl-left-margin">8. </div><div class="csl-right-inline">Weidmann, N. B. &amp; Gleditsch, K. S. <em>Cshapes: <span>The CShapes Dataset</span> and <span>Utilities</span></em>. (2016).</div>
</div>
<div id="ref-Coppedge2021" class="csl-entry">
<div class="csl-left-margin">9. </div><div class="csl-right-inline">Coppedge, M. <em>et al.</em> <em>V-<span>Dem Country Coding Units</span> V11.1</em>. 50 <a href="https://www.v-dem.net/media/filer_public/8b/2a/8b2a2a32-cd62-4a2b-8cd5-f52974032c8e/countryunit_v111.pdf">https://www.v-dem.net/media/filer_public/8b/2a/8b2a2a32-cd62-4a2b-8cd5-f52974032c8e/countryunit_v111.pdf</a> (2021).</div>
</div>
</div>
</div>

<footer>

</footer>

</body>

</html>
