
# danteSubmit <img src='man/figures/logo.png' align="right" height="150"/>

`danteSubmit` is an R package that disperses HTML templates for [DANTE
Project](www.dante-project.com) datasets and vignettes submissions.
Future update may add support for function and package submission in
addition to additional outputs (LaTeX - PDF). Package updates are
tracked in the
[newsfeed](https://gitlab.com/dante-sttr/dantesubmit/blob/master/NEWS.md).

The following presents a brief introduction to the `danteSubmit`
package. For more detailed information and reference materials for
`danteSubmit` and tutorials for contributing to the DANTE project,
please refer to:

  - [danteSubmit Reference
    Manual](https://dante-sttr.gitlab.io/dantesubmit/index.html)
  - [Submitting a Dataset to
    DANTE](https://dante-sttr.gitlab.io/dantesubmit/articles/danteDataset.html)
  - [Submitting a Function to
    DANTE](https://dante-sttr.gitlab.io/dantesubmit/articles/danteFunction.html)
  - [Submitting a Vignette to
    DANTE](https://dante-sttr.gitlab.io/dantesubmit/articles/danteVignette.html)
  - [Creating and Populating a New Git
    Repository](https://dante-sttr.gitlab.io/dantesubmit/articles/dante-new-git.html)
  - [Establishing R Libraries on Windows Systems with Active
    Directory](https://dante-sttr.gitlab.io/dantesubmit/articles/windows-pkg-inst.html)

## Template Features

The `danteSubmit` templates provide custom meta-data fields for several
relevant fields to the DANTE-Project. The custom fields provide
discussion and cross referencing for a variety of DANTE-Project
datasets, R packages, and citations. All metadata fields available are
listed in the provided templates. If the fields are unknown or otherwise
irrelevant they may be ignored or deleted from the template. Deleting
superflous fields will present a more readable document when compiled
into HTML. The additional metadata fields include but are not limited
to:

  - Author affiliations and contact information.
  - R packages used in the document.
  - Datasets referenced or utilized in the document.
  - Relevant data types used in the document.
  - Related vignettes to the document.
  - Research highlights found in the document.

## Installation

You can install the current version of danteSubmit from
[GitLab](https://gitlab.com/dante-sttr/danteSubmit) with:

``` r
devtools::install_gitlab("dante-sttr/danteSubmit")
```

### Installation With Windows

To install R packages over Git on a Windows system, you must install
Rtools first. The latest version of Rtools is available
[here](https://cran.r-project.org/bin/windows/Rtools/). Furthermore, you
may experience difficulty installing R packages over Git if you utilize
a Windows machine on a network with Active Directory or shared network
drives. To enable proper package installation under these circumstances
please follow [this
guide](https://dante-sttr.gitlab.io/dante-vignettes/windows-pkg-inst/windows-pkg-inst.html).

## Accessing the Templates

After installing `danteSubmit` the templates will be accessible through
the RStudio interface.

``` r
library(danteSubmit)
```

To begin a submission select `File > New File > R Markdown...`

<div style="text-align:center">

<img src="man/figures/new_markdown.PNG" />

</div>

The `New R Markdown` interface will appear. Select `From Template` and
the `danteSubmit` templates will be available for
use.

<div style="text-align:center">

<img src="man/figures/dantesubmit_template.PNG" />

</div>

<hr>

<footer style="display: table; text-align:center; margin-left: auto; margin-right: auto;">

<p style="color:red;">

This work was developed in support of
<a rel="association" href="www.dante-project.org"><img align="center" height="50" src="man/figures/dante-logo-v1.png"/></a>

</p>

</footer>
