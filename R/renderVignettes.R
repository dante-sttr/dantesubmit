#' Render Dante Vignettes
#'
#' Convert all current Dante Vignettes into custom Dante HTML output files
#' for back-end web translation.
#'
#' @param check.changes Logical value that will force \code{renderVignettes} to
#'   check for file changes, and only render those that have changed since the
#'   last \code{renderVignette}. Default value is \code{check.changes=TRUE}.
#'
#' @return HTML files and accompanying sub-directories of page files and images.
#' @export
renderVignettes <- function(check.changes = TRUE) {

  library(magrittr)

  vignettes <- data.table::data.table(
    vignettes = list.files(
      "web-content/walkthroughs/",
      ".Rmd$",
      full.names = TRUE,
      recursive = TRUE,
      include.dirs = TRUE,
    ),
    root = list.dirs("web-content/walkthroughs/", recursive = FALSE)
  )

  current.info <-
    data.table::setDT(cbind(vignettes,
                            file.info(vignettes$vignettes)))
  vignette.info<-data.table::copy(current.info)

  if(check.changes==TRUE){

    current.info <-
      data.table::setDT(cbind(vignettes,
                              file.info(vignettes$vignettes)))

    compare.info <-
      merge(
        danteSubmit::vignette.info,
        current.info,
        by = c("vignettes", "root"),
        all = TRUE
      )
    compare.info[,change:= mtime.x!=mtime.y]
    compare.info[is.na(mtime.x),change:=TRUE]

    changed.vignettes<-as.character(compare.info[change==TRUE,vignettes])
    if (length(changed.vignettes) == 0) {
      stop(
        "There are no detected changes in vignettes; aborting process. If you believe
           this message is in error, please contact Josh Brinks (jbrinks@isciences.com)
           or fix it yourself."
      )
    }

    vignette.info<-data.table::copy(current.info)
    vignettes<-vignette.info[vignettes %in% changed.vignettes]


  }

    for (vignette in 1:nrow(vignettes)) {
    rmarkdown::render(
      input = as.character(vignettes[vignette, "vignettes"]),
      output_dir = gsub("web-content/walkthroughs//", "vignette-out/", vignettes[vignette, "root"])
    )

    # Read html output for DANTE layout imagery----
    vignette.html <-
      gsub("web-content/walkthroughs//",
           "vignette-out/",
           as.character(vignettes[vignette, "vignettes"]))

    vignette.html <- gsub(".Rmd", ".html", vignette.html)

    vignette.html <-
      xml2::read_html(vignette.html)

    ## First the browse image----

    vignette.browse.image <- vignette.html %>%
      rvest::html_nodes('#browse-image') %>%
      rvest::html_attr("content")

    vignette.browse.image <-
      paste0(as.character(vignettes[vignette, "root"]), "/", vignette.browse.image)

    file.copy(
      from = vignette.browse.image,
      to = gsub(
        "web-content/walkthroughs//",
        "vignette-out/",
        as.character(vignettes[vignette, "root"])
      ),
      overwrite = TRUE
    )

    ## Now the subheading image----

    vignette.subhead.image <- vignette.html %>%
      rvest::html_nodes('#subhead-image') %>%
      rvest::html_attr("content")

    vignette.subhead.image <-
      paste0(as.character(vignettes[vignette, "root"]), "/", vignette.subhead.image)

    file.copy(
      from = vignette.subhead.image,
      to = gsub(
        "web-content/walkthroughs//",
        "vignette-out/",
        as.character(vignettes[vignette, "root"])
      ),
      overwrite = TRUE
    )

    }

  usethis::use_data(vignette.info, overwrite = TRUE)

  message(
    "You have updated the vignette file info database. Please remember to
            Install and Restart danteSubmit to update your changes locally, and
            push your changes to the GitLab repository."
  )

}


