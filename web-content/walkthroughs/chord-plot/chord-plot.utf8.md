---
title: "Creating Chord Diagrams for Dyadic Data"

metadata-date: "June 7, 2021"

author: 
- name: Joshua Brinks
  email: jbrinks@isciences.com
  affiliation: ISciences, LLC

vignette-highlights:
- Dyadic data is commonplace in political science and geography, but there are limited options for visualization.
- Although chord diagrams are a great option for visualization, they can be more complicate to create. 
- We walk through creating a chord diagram with R's circlize package. 

related-packages:
- <a href="https://jokergoo.github.io/circlize_book/book/">circlize</a>
- <a href="/untools">untools</a>

related-datasets:
- <a href="/datasets/unhcr-ts">UHCR Populations of Concern - Time Series</a>

keywords:
- visualization

browse-image: "browse-photo.jpg"
subhead-image: "subhead-image.jpg"
image-attribution: <span>Photo by <a href="https://www.pexels.com/@iamsusndybvik">Susn Dybvik</a> on <a href="https://www.pexels.com/photo/modern-art-3608328/">Pexels</a></span>

bibliography: "chord-plot.bib"

output:
  danteSubmit::danteVignette
---

## Introduction

Dyadic data can be found in every corner of the environment-security research sector. This is no surprise, because more often than not in the eco-security sector we're simply analyzing interactions between countries or other municipalities. Whether you're working with IMF's Direction of Trade @InternationalMonetaryFund2018, UCDP Conflict @Sundberg2012, Militarized Interstate Disputes @Maoz2018, ICEWS @Boschee2018 and GDELT @Leetaru2013 event data, UN Migrant Stocks @TheUnitedNations2015, UNHCR Populations of Concern, U.S. Agricultural Trade [@U.S.DepartmentofAgriculture2021], or countless other datasets spanning the geographical, political, and environmental sciences.

Despite the prevalence of dyadic data, there are limited options for compelling visualizations. Tables are a natural fit, but they're not fun, and your eyes will start to glaze over as the number dyadic pairs increase. Barplots and linegraphs can be used to highlight specific pairs or subsets of dyadic pairs, but you need something a little more to visualize a multitude of interactions. Two of the most popular options are Sankey and chord diagrams.

Sankey diagrams visualize flows between entities with the option of being able to depict flows through time. These can be created with R using the [networkD3](https://christophergandrud.github.io/networkD3/) and [plotly](https://plotly.com/r/) packages. Chord diagrams use a circular structure to visualize dyadic flows, however, they do not handle time series data unless you opt to animate the plot. The most popular package to create chord plots in R is [circlize](https://jokergoo.github.io/circlize_book/book/) @Brors2014.

**circlize** is a well developed package originally designed to visualize genomic data. Chord diagrams are only a small part of the packages overall capabilities, and there is an accompanying bookdown reference manual detailing all of its potential. Although there is a plethora of documentation for using **circlize**, it can be cumbersome to create figures just as you imagine them. This is because **circlize** relies on Base R Graphics (*screams*)! If, like myself, your visualization workflows have become engulfed by the ggplot2 universe over the past decade, it can be real tedious jumping back into Base R plotting. Chapters [14 and 15](https://jokergoo.github.io/circlize_book/book/the-chorddiagram-function.html) of the **circlize** manual review the chord diagram functionality, but I found that it still took a bit of internet searching and trial and error to get what I initially envisioned.

My intent with this guide is to walk through each step of creating a chord diagram using **circlize**.

## The Data

For this example I chose the United Nations High Commissioner on Refugees Populations of Concern dataset. I wanted to focus on visualizing, not processing, the data. In comparison to some other dyadic datasets, UNHCR refugee and asylum data does not require as much preparation.

### Acquiring the Data

You can download the data from the [UNHCR Refugee Data Finder](https://www.unhcr.org/refugee-statistics/download/?url=E1ZxP4) or using DANTE's untools package. For this example, I'm going to access the data programmatically with untools, and perform some light processing using **data.table**.

You can install the released version of untools from [GitLab](https://https//gitlab.com/dante-sttr/untools) with:


```r
library(data.table)
devtools::install_gitlab("dante-sttr/untools", dependencies=TRUE)
```

```
## Downloading GitLab repo dante-sttr/untools@HEAD
## from URL https://gitlab.com/api/v4/projects/12030310/repository/archive.tar.gz?sha=HEAD
```

```
## commonCodes  (0.1.0      -> 6ff087721...) [GitLab]
## fansi        (0.4.2      -> 0.5.0       ) [CRAN]
## RcppArmad... (0.10.4.0.0 -> 0.10.5.0.0  ) [CRAN]
## pillar       (1.6.0      -> 1.6.1       ) [CRAN]
## dplyr        (1.0.5      -> 1.0.6       ) [CRAN]
## tibble       (3.1.1      -> 3.1.2       ) [CRAN]
## colorspace   (2.0-0      -> 2.0-1       ) [CRAN]
## systemfonts  (1.0.1      -> 1.0.2       ) [CRAN]
## stringi      (1.5.3      -> 1.6.2       ) [CRAN]
## xfun         (0.22       -> 0.23        ) [CRAN]
## tinytex      (0.31       -> 0.32        ) [CRAN]
## viridis      (0.6.0      -> 0.6.1       ) [CRAN]
## rmarkdown    (2.7        -> 2.8         ) [CRAN]
## zip          (2.1.1      -> 2.2.0       ) [CRAN]
```

```
## Installing 13 packages: fansi, RcppArmadillo, pillar, dplyr, tibble, colorspace, systemfonts, stringi, xfun, tinytex, viridis, rmarkdown, zip
```

```
## Installing packages into 'C:/Users/jbrin/Documents/R/win-library/4.0'
## (as 'lib' is unspecified)
```

```
## 
##   There is a binary version available but the source version is later:
##         binary source needs_compilation
## stringi  1.5.3  1.6.2              TRUE
## 
## package 'fansi' successfully unpacked and MD5 sums checked
```

```
## Warning: cannot remove prior installation of package 'fansi'
```

```
## Warning in file.copy(savedcopy, lib, recursive = TRUE): problem copying C:
## \Users\jbrin\Documents\R\win-library\4.0\00LOCK\fansi\libs\x64\fansi.dll to C:
## \Users\jbrin\Documents\R\win-library\4.0\fansi\libs\x64\fansi.dll: Permission
## denied
```

```
## Warning: restored 'fansi'
```

```
## package 'RcppArmadillo' successfully unpacked and MD5 sums checked
## package 'pillar' successfully unpacked and MD5 sums checked
## package 'dplyr' successfully unpacked and MD5 sums checked
```

```
## Warning: cannot remove prior installation of package 'dplyr'
```

```
## Warning in file.copy(savedcopy, lib, recursive = TRUE): problem copying C:
## \Users\jbrin\Documents\R\win-library\4.0\00LOCK\dplyr\libs\x64\dplyr.dll to C:
## \Users\jbrin\Documents\R\win-library\4.0\dplyr\libs\x64\dplyr.dll: Permission
## denied
```

```
## Warning: restored 'dplyr'
```

```
## package 'tibble' successfully unpacked and MD5 sums checked
```

```
## Warning: cannot remove prior installation of package 'tibble'
```

```
## Warning in file.copy(savedcopy, lib, recursive = TRUE): problem copying C:
## \Users\jbrin\Documents\R\win-library\4.0\00LOCK\tibble\libs\x64\tibble.dll to C:
## \Users\jbrin\Documents\R\win-library\4.0\tibble\libs\x64\tibble.dll: Permission
## denied
```

```
## Warning: restored 'tibble'
```

```
## package 'colorspace' successfully unpacked and MD5 sums checked
## package 'systemfonts' successfully unpacked and MD5 sums checked
## package 'xfun' successfully unpacked and MD5 sums checked
```

```
## Warning: cannot remove prior installation of package 'xfun'
```

```
## Warning in file.copy(savedcopy, lib, recursive = TRUE): problem copying C:
## \Users\jbrin\Documents\R\win-library\4.0\00LOCK\xfun\libs\x64\xfun.dll to C:
## \Users\jbrin\Documents\R\win-library\4.0\xfun\libs\x64\xfun.dll: Permission
## denied
```

```
## Warning: restored 'xfun'
```

```
## package 'tinytex' successfully unpacked and MD5 sums checked
## package 'viridis' successfully unpacked and MD5 sums checked
## package 'rmarkdown' successfully unpacked and MD5 sums checked
## package 'zip' successfully unpacked and MD5 sums checked
## 
## The downloaded binary packages are in
## 	C:\Users\jbrin\AppData\Local\Temp\RtmpeOTk4j\downloaded_packages
```

```
## installing the source package 'stringi'
```

```
## Warning in i.p(...): installation of package 'stringi' had non-zero exit status
```

```
## Downloading GitLab repo dante-sttr/commonCodes@HEAD
## from URL https://gitlab.com/api/v4/projects/12810804/repository/archive.tar.gz?sha=HEAD
```

```
## 
##   
  
  
v  checking for file 'C:\Users\jbrin\AppData\Local\Temp\RtmpeOTk4j\remotes3a7891a828\commoncodes-HEAD-6ff087721459abd425d0f635ad69232a744cb0f3/DESCRIPTION'
## 
  
  
  
-  preparing 'commonCodes': (436ms)
##    checking DESCRIPTION meta-information ...
  
   checking DESCRIPTION meta-information ... 
  
v  checking DESCRIPTION meta-information
## 
  
  
  
-  checking for LF line-endings in source and make files and shell scripts
## 
  
  
  
-  checking for empty or unneeded directories
## 
  
  
  
-  building 'commonCodes_0.1.0.tar.gz'
## 
  
   
## 
```

```
## Installing package into 'C:/Users/jbrin/Documents/R/win-library/4.0'
## (as 'lib' is unspecified)
```

```
##   
  
  
   checking for file 'C:\Users\jbrin\AppData\Local\Temp\RtmpeOTk4j\remotes3a7851ea4093\untools-HEAD-45596c75634ffbcbed7447009764401e17a80069/DESCRIPTION' ...
  
   checking for file 'C:\Users\jbrin\AppData\Local\Temp\RtmpeOTk4j\remotes3a7851ea4093\untools-HEAD-45596c75634ffbcbed7447009764401e17a80069/DESCRIPTION' ... 
  
v  checking for file 'C:\Users\jbrin\AppData\Local\Temp\RtmpeOTk4j\remotes3a7851ea4093\untools-HEAD-45596c75634ffbcbed7447009764401e17a80069/DESCRIPTION'
## 
  
  
  
-  preparing 'untools': (854ms)
##    checking DESCRIPTION meta-information ...
  
   checking DESCRIPTION meta-information ... 
  
v  checking DESCRIPTION meta-information
## 
  
  
  
-  checking for LF line-endings in source and make files and shell scripts
## 
  
  
  
-  checking for empty or unneeded directories
## 
  
   Removed empty directory 'untools/data-raw'
## 
  
Removed empty directory 
  
   Removed empty directory 'untools/docs/articles/explore-unhcr_files/figure-html'
##    Removed empty directory 'untools/docs/articles/explore-unhcr_files'
## 
  
Removed empty directory 
  
   Removed empty directory 'untools/docs/articles/explore-unstocks_files/figure-html'
##    Removed empty directory 'untools/docs/articles/explore-unstocks_files'
## 
  
   Removed empty directory 'untools/docs/articles/geo-unref_files/figure-html'
## 
  
   Removed empty directory 'untools/docs/articles/geo-unref_files'
## 
  
  
  
-  building 'untools_0.2.0.tar.gz'
## 
  
   
## 
```

```
## Installing package into 'C:/Users/jbrin/Documents/R/win-library/4.0'
## (as 'lib' is unspecified)
```

You can download and load the refugee data into your global environment with the following call.


```r
unref<-untools::getUNref()
```

```
## Making UNHCR API request for page: 1
```

```
## Parsing response...
```

```
## Transposing parsed json list...
```

```
## Complete.
```

```
## Making UNHCR API request for page: 2
```

```
## Parsing response...
```

```
## Transposing parsed json list...
```

```
## Complete.
```

```
## Making UNHCR API request for page: 3
```

```
## Parsing response...
```

```
## Transposing parsed json list...
```

```
## Complete.
```

```
## Making UNHCR API request for page: 4
```

```
## Parsing response...
```

```
## Transposing parsed json list...
```

```
## Complete.
```

This is a large dataset with several fields. UNHCR does not provide a lot of documentation, but you can review the variable descriptions in the `getUNref()` [help documentation](https://dante-sttr.gitlab.io/untools/reference/getUNref.html). For simplicity, we will only visualize refugees so we can start off by subsetting the data for the refugee totals, the year of observation, and some dyad identifiers. The sender country is `coo_name` (country of origin) and the receiving country is `coa_name` (country of asylum). The `_iso` suffixes refer to the respective ISO3 character codes; these help us join the data to any additional information we might want to include from external datasets.


```r
keeps<-c("coo_name", "coo_iso", "coa_name", "coa_iso", "year", "refugees")
unref<-unref[ ,..keeps]
```

The time series also goes back to 1950. We'll make it more manageable (and trustworthy) by restricting it to 2000-2019 (last year of available data).


```r
unref<-unref[year %in% seq(2000, 2019)]
```

This still includes more than 84,000 records, but more importantly it contains 7761 unique sender and receiver countries.

# References
