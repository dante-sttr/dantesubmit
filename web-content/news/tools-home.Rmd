---
title: "tools-home"
author: "Joshua Brinks"
date: "6/4/2021"
output: html_document
---

The DANTE toolkit aims to address a range of technical issues that present common barriers to data analysts working within the interdisciplinary environment-security research sector. While there is an abundance of web based mapping and other exploratory data services, there is a dearth of tools and materials for intermediate or advanced users. At present DANTE maintains multiple R packages developed to assist with laborious tasks in applied political, environmental, and geo sciences. These include:

-   Provide ready access to relevant **literature** documenting analytic methods, datasets, and toolkits.

-   Acquire and maintain regularly updated [**datasets**](https://www.dante-project.org/datasets).

-   **Harmonize** datasets by performing country/administrative code matching, translation between incompatible units of analysis, and resolving differences in temporal and spatial resolution.

-   Perform **integrated analysis** of tabular, geospatial feature, and geospatial raster data.

-   Relay and demonstrate the principles of open science and reproducible research.

-   Provide well documented vignettes and end-to-end **analytic use cases.**

Although DANTE currently focuses on the R statistical computing platform, contributions from other open source platforms are welcomed. The DANTE project has multiple R packages in early development. 

+---------------------------------------------------------------------------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------+
| [![](https://www.dante-project.org/sites/default/files/dspages/package-info/demcon-logo.png)](https://www.dante-project.org/demcon)         | [![](https://www.dante-project.org/sites/default/files/dspages/package-info/untools-logo.png)](https://www.dante-project.org/untools)         | [![](https://www.dante-project.org/sites/default/files/dspages/package-info/exactextractr-logo.png)](https://www.dante-project.org/exactextractr) |
|                                                                                                                                             |                                                                                                                                               |                                                                                                                                                   |
| Acquire, process, and visualize political data                                                                                              | Access UN migration data from their API                                                                                                       | Fast and accurate zonal statistics with R                                                                                                         |
+:===========================================================================================================================================:+:=============================================================================================================================================:+:=================================================================================================================================================:+
|                                                                                                                                             |                                                                                                                                               |                                                                                                                                                   |
|                                                                                                                                             |                                                                                                                                               |                                                                                                                                                   |
| [![](https://www.dante-project.org/sites/default/files/dspages/package-info/wsim-logo-small.png)](https://www.dante-project.org/wsim)       | [![](https://www.dante-project.org/sites/default/files/dspages/package-info/sedacr-logo-small.png)](https://www.dante-project.org/sedacr)     | [![](https://www.dante-project.org/sites/default/files/dspages/package-info/commoncodes-logo.png)](https://www.dante-project.org/commoncodes)     |
|                                                                                                                                             |                                                                                                                                               |                                                                                                                                                   |
| Water Security Indicator Model (WSIM)                                                                                                       | Connect to the SEDAC API                                                                                                                      | Harmonize country codes and classifications                                                                                                       |
+---------------------------------------------------------------------------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------+
|                                                                                                                                             |                                                                                                                                               |                                                                                                                                                   |
|                                                                                                                                             |                                                                                                                                               |                                                                                                                                                   |
| [![](https://www.dante-project.org/sites/default/files/dspages/package-info/duplicator-logo.png)](https://www.dante-project.org/duplicator) | [![](https://www.dante-project.org/sites/default/files/dspages/package-info/dantesubmit-logo.png)](https://www.dante-project.org/dantesubmit) | [![](https://www.dante-project.org/sites/default/files/dspages/package-info/conflictr-logo.png)](https://www.dante-project.org/conflictr)         |
|                                                                                                                                             |                                                                                                                                               |                                                                                                                                                   |
| Demonstrate open and reproducible science                                                                                                   | Contribute to the DANTE Project                                                                                                               | Acquire international conflict data                                                                                                               |
+---------------------------------------------------------------------------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------+

# External Tools and Packages

------------------------------------------------------------------------

Navigating the plethora of packages and tools for interdisciplinary research sectors like environment-security can be a daunting task. To assist researchers and analysts, we maintain a list of tools and packages that are widely considered best practices in each of the listed sub-disciplines.

+--------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------+---+
| **Data Processing**                                                                                    | **Political Science**                                                |   |
|                                                                                                        |                                                                      |   |
| -   [dplyr](https://dplyr.tidyverse.org/) and the rest of the [tidyverse](https://www.tidyverse.org/). | -   [countrycode](https://vincentarelbundock.github.io/countrycode/) |   |
|                                                                                                        |                                                                      |   |
| -   [data.table](https://rdatatable.gitlab.io/data.table/)                                             | -   [states](https://www.andybeger.com/states/)                      |   |
|                                                                                                        |                                                                      |   |
|                                                                                                        | -   vdemdata                                                         |   |
|                                                                                                        |                                                                      |   |
|                                                                                                        | -                                                                    |   |
+--------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------+---+
|                                                                                                        |                                                                      |   |
+--------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------+---+
|                                                                                                        |                                                                      |   |
+--------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------+---+
