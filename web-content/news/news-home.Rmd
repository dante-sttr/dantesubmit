---
title: "news"
author: "Joshua Brinks"
date: "6/4/2021"
output: html_document
---

**18-21 May 2021**

The NYU Center on International Cooperation recently held the [Conflict Early Warning/Early Action Practitioners Workshop](https://dataforpeace.conference.tc/conflictearlywarningearlyaction/c/home-tZA8ZfAcfASaK1b8Gqmnvf/welcome-qkzBimCribST6LmT9Mnus9/). Read about some interesting sessions in [DANTE's debriefing](/news/ewea-05-21).

**11 April 2021**

DANTE outlined their new platform a the [Association of American Geographer's 2021 Annual Meeting](https://www.youtube.com/watch?v=n2KCJRMl8yk&t=7s).

**22-25 March 2021**

As part of its [Case for Democracy program](https://www.v-dem.net/en/our-work/research-projects/case-democracy/), the V-Dem institute held [Democracy Week 2021](https://www.v-dem.net/media/filer_public/22/67/2267c7aa-44f9-4ae6-b08e-71130368c618/cfd_week_2021_short_program_open_v2.pdf). Learn about one of the environment-security related session in [DANTE's debriefing](/news/dem-week-0325).

**4 December 2020**

Save the date! Tuesday, 15 December 2020. Join us at AGU2020 for SY052 [*Earth Intel: Enhancing Partnerships Between Academic and National Security Communities to Address Ecosecurity Challenges*](https://agu.confex.com/agu/fm20/meetingapp.cgi/Session/106858). 11:30am-12:30pm Eastern Time. We've got a great line up of speakers for the panel on environment-security analysis at AGU2020.

**3 December 2020**

Join us a AGU2020! SY028-02 [*Decision Support Tools to Communicate Hydrologic Anomalies and Impacts*](https://agu.confex.com/agu/fm20/meetingapp.cgi/Paper/757429). Wednesday, 9 December 2020, 10:00-11:00am Eastern Time. Featuring ISciences' work on the Water Security Indicator Model (WSIM) in this joint ISciences and ArmyERDC paper.

**2 December 2020**

Join us at AGU2020! SY028 [*Data for All: Open Data Sharing and Analytics to Empower Science eLightning*](https://agu.confex.com/agu/fm20/meetingapp.cgi/Session/108172). Wednesday, 9 December 2020, 10:00-11:00am Eastern Time.

**23-25 September 2020**

DANTE presented and update on their developing platform at the International Symposium "[Global Collaboration on Data beyond Disciplines](https://ds.rois.ac.jp/article/dsws_2020/)".

**21 July 2020**

DANTE conducted an Environment-Security Discussion Panel and Practitioners Workshop at the ESIP 2020 Summer Virtual Meeting. View the session info at [Open-Source Environment-Security Analytics: Combining Capabilities from Government, Industry, and Academia](https://2020esipsummermeeting.sched.com/event/cIu5/open-source-environment-security-analytics-combining-capabilities-from-government-industry-and-academia-part-1?iframe=no) or watch the live recording on [YouTube](https://www.youtube.com/watch?v=XCYTVRWijx8). 

**20 May 2019**

DANTE at Climate Change, Human Migration, and Health Conference

The DANTE Project, Data Analytics and Tools for Ecosecurity, will be a featured poster presentation at the upcoming "Climate Change, Human Migration, and Health" conference May 20-21, 2019 at the University of Colorado Boulder.

Climate change is influencing human migration patterns while also impacting human health. Innovations in the integration of social and ecological data are essential to move forward these critical research frontiers, as well as to investigate other human dimensions of global environmental change. The conference will include speakers reviewing challenges and innovations in socio-ecological data integration, research panels, and a poster reception showcasing empirical examples. 

Susana Adamo from Columbia University will present the DANTE poster, "An Open Community Platform for Environment and Security Research and Development." DANTE is a two-year research and development effort to provide an open-source software toolkit for systematic monitoring, forecasting and analysis of environmental stresses and their impacts on security outcomes. The initial focus for DANTE will address the role of environmental stressors in three key areas: international migration and refugee flows; internal migration and isolated populations; and conflict and political instability. 

The DANTE Project is a partnership formed by ISciences, L.L.C., CIESIN at Columbia University, and CASE Consultants International, with generous support from the U.S. Army Engineer Research and Development Center (ERDC). For more information about DANTE, please visit <https://www.dante-project.org/>

The conference is supported and organized by the University of Colorado Population Center, the International Union for the Scientific Study of Population, and CU Boulder's Institute of Behavioral Science, Grand Challenge and Earth Lab.

**4 March 2019**

DATA ANALYTICS AND TOOLS FOR ECOSECURITY (DANTE) PROJECT LAUNCHED

New Open-Source Software Platform Aims to Accelerate Environment and Security Analysis

ANN ARBOR, Mich. (March 4, 2019) -- ISciences announced today the start of the Data ANalytics and Tools for Ecosecurity (DANTE) project. A joint effort of ISciences, the Center for International Earth Science Information Network (CIESIN) and North Carolina-based CASE Consultants International, DANTE aims to provide scientists, defense and security analysts, relief agencies, businesses and communities with enhanced insights into the connections between environmental stresses and security outcomes. The first of three project phases will be released under open-source licensing in August 2019.

As noted in the 2019 U.S. National Intelligence Strategy, "growing influxes of migrants, refugees and internally-displaced persons fleeing conflict zones, areas of intense economic or other resources scarcity, and areas threatened by climate change, [and] infectious disease outbreaks" are "straining the capacities of governments around the world and are likely to result in fracturing of societies." DANTE will provide open-source software tools designed to accelerate quantitative interdisciplinary analysis of environmental stresses, demographics, economics, health, conflict areas, disaster response and national boundaries enabling analysts to better anticipate conditions that may require humanitarian relief, disaster recovery investments and conflict management responses.

"We believe in the power of evidence-based analysis to understand vulnerabilities and achieve sustainability in a rapidly changing world," said ISciences President, Thomas M. Parris, adding, "the core strength of ISciences, LLC is the ability to bring together expertise in the physical sciences, the social sciences, and information technology to find solutions. With the launch of DANTE in partnership with CIESIN and CASE Consultants, we expect to democratize access to state-of-the-art tools for environment and security analysis and contribute to a more productive analytical enterprise."

DANTE is a two-year research and development effort to provide an open-source software toolkit for systematic monitoring, forecasting and analysis of environmental stresses and their impacts on security outcomes. The initial focus for DANTE will address the role of environmental stressors in three key areas: international migration and refugee flows; internal migration and isolated populations; and conflict and political instability. For more information about DANTE, please visit <https://www.dante-project.org/>

ISciences provides strategic, scientific and technical consulting services to government, commercial and non-profit clients in the fields of water and climate, corporate sustainability, remote sensing, and human security. Our contributions influence policy decisions at some of the world's largest companies and at the highest reaches of government. For more information about ISciences, please visit <https://www.isciences.com/>.

CIESIN works at the intersection of the social, natural, and information sciences, and specializes in scientific data and information management, spatial data integration, and interdisciplinary research related to human interactions in the environment. For more information about CIESIN, please visit <http://www.ciesin.columbia.edu/>.

CASE Consultants International works to integrate science-based decision making into the practices of climate-sensitive professions; and, informs decisions in sustainability, resilience, and climate adaptation. For more information about CASE Consultants International, please visit <https://www.caseconsultantsinternational.com/>.
