---
author: 
  - name: Joshua Brinks
    email: jbrinks@isciences.com
    affiliation: ISciences, LLC

metadata-date: "February 24, 2020"

citation-information:
    title: Natural Earth (Countries)
    edition: Version 4.1.0
    publication-date: 21 May 2018
    geospatial-data-presentation-form: shapefiles, SQlite, GeoPackage
    publisher: Natural Earth, North American Cartographic Information Society
    online-linkage: https://www.naturalearthdata.com/downloads/
    dante-citekey: Kelso2010a

dataset-highlights:
  - Large complimentary feature datasets. 
  - Free for all uses.
  - No ADMIN 2 representation.
  - Some boundary accuracy issues, bad geometries, and topology errors that need to be post-processed.

abstract: "Internal, first-order administrative boundaries and polygons for all but a few tiny countries. Includes name attributes (including diacritical marks), name variants, and some statistical codes (FIPS, ISO, HASC)."

use-constraints: "Free for all use."

spatial-information:
  bounding-coordinates:
    west-bounding-coordinate: -179.9999999999999147
    east-bounding-coordinate: 180.00
    north-bounding-coordinate: 83.6341006530000755
    south-bounding-coordinate: -89.9999999999999432
  spatial-reference-information:
    coordinate-system: Latitude and Longitude
    geodetic-model: WGS1984
    
related-packages:
  - <a href="https://cran.r-project.org/web/packages/rnaturalearth/index.html">rnaturalearth</a>

related-datasets:
- <a href="/datasets/gadm">Database of Global Administrative Boundaries (GADM)</a>
- <a href="/datasets/un-gaul">Global Administrative Units Layer</a>
- <a href="/datasets/cshapes">cshapes</a>

related-vignettes:

bibliography: "boundaries.bib"

browse-image: "nat-earth-browse.jpg"
subhead-image: "nat-earth-subhead.jpg"

output: 
  danteSubmit::danteDataset
---
While it is often an overlooked aspect of research and commercial applications, joining tabular country data or performing statistical summaries of raster and vector feature data within country boundaries is a key component of geographical, political, climate, and social sciences. The Natural Earth database [NE @Kelso2010a] is one of a small number of vectorized global municipal boundaries. Additional global vectorized municipal boundaries are available from The Database of Global Administrative Boundaries (GADM [@UniversityofCaliforniaBerkley2018]), Esri/Garmin [@EsriGarminInternationalInc.2020], the Food and Agricultural Organization (GAUL [@FoodandAgriculturalOrganizationoftheUnitedNations2015]), the United Nations (SALB [@TheUnitedNations2015a]), and CShapes [@Weidmann2010c].

NE is the only fully open-source and public domain pre-packaged database of vectorized administrative boundaries. Natural Earth presents global coverage of ADMIN 0 and ADMIN 1 boundaries, but no ADMIN 2 coverage and is available as downloadable shapefiles, SQlite, and GeoPackage, or a limited number of features via the `rnaturalfeatures` R package. Unlike GADM, GAUL, and SALB, Natural Earth also features the premiere collection of additional public domain [cultural and physical vector data themes](https://www.naturalearthdata.com/features/). These include, but are not limited too, disputed areas, urban areas, parks and protected areas, water boundaries, coastlines, reefs, lakes, and bathymetry. Although NE delivers the largest collection of public domain vectorized boundaries layers, users should be aware of limited data quality issues. There are sparse instances of boundary overlaps, bad geometries, and topology errors that may cause processing errors in some GIS software or packages. The Esri/Garmin World Countries dataset suffers from similar issues.

```{r echo=FALSE, message=FALSE, warning=FALSE, fig.cap="Figure 1: Global zero order administrative boundaries as specified by the Natural Earth Database."}
world <- rnaturalearth::ne_countries(scale = "medium", returnclass = "sf")
 ggplot2::ggplot(data = world, ggplot2::aes(fill=sovereignt))+
   ggplot2::geom_sf(lwd=0)+
   ggplot2::scale_fill_discrete(guide=FALSE)

```

GADM is available as a standalone vectorized shapefile layer and is also distributed through the R `raster` package. GADM presents highly detailed boundaries to ADMIN 2 municipal subdivisions (U.S. county equivalent), while Esri/Garmin databases offer ADMIN 1 boundaries (U.S. States), Natural Earth provides ADMIN 1 boundaries for a limited number of countries, and GAUL and SALB provide ADMIN 1 and ADMIN 2 boundaries for select nation-states. Although GADM's level of detail and delineation is superior to its competitors, there are drawbacks. The high resolution, specifically along coastlines, is often cumbersome when processing the data at large spatial extents with geospatial software. GADM boundaries may require simplification procedures to reduce processing times. Furthermore, GADM boundaries are not validated by any federal or international entity; consequently, while they are detailed, they are not necessarily accurate. A 2011 comparison of vectorized global administrative boundaries found that GADM boundaries were less accurate than SALB and GAUL [@Brigham2011]. For personal use and research exercises, the lack of validation is likely inconsequential; it's also not commonplace in most geographic boundary databases. SALB promotes federally validated geo-databases, but participating nation-state boundaries are decentralized and must be acquired individually through the SALB web portal that links to federal geospatial repositories. GADM nation-state layers are aggregated into a singular data product that is more convenient for most applications.

Whereas the aforementioned data sets are fantastic resources for maps and current analysis, they provide no resources for historic visualizations or time-series analysis of spatially explicit country-year processes. The CShapes dataset presents ADMIN 0 boundaries, however, this is not the primary focus; the purpose of CShapes is to present *historical* state boundaries dating back to 1946 [@Weidmann2010c]. This presents a few interesting questions: 1) what defines an independent state in this context, 2) what defines the spatial extent of a state, and 3) what constitutes a change in state boundaries? Weidmann (2010) [@Weidmann2010c] wisely chose to offload recognition of an independent state by relying on 2 well established lists of states born out of political science and conflict data sets: 1) Gleditsch (1999) [@Gleditsch1999] and 2) the Correlates of War Project [@CorrelatesofWarProject2017]. Most nation-state boundary data sets take practical approaches to the spatial extent of a state by relying on internationally accepted boundaries. States may often make wild claims about their territories (see Venezuela), however, as a practical matter, the actual boundaries are those by which they exert control over. This component of the CShapes dataset was developed internally and is described in detail within Weidmann (2010) [@Weidmann2010c]. Lastly, CShapes relays temporal territorial changes by observing the merger, emergence, disappearance, or dissolution of independent states. These distinctions are also outlined in detail in Weidmann (2010) [@Weidmann2010c]. CShapes is available as a standalone vectorized shapefile and a package for the R software package. As an added bonus, the `cshapes` R package offers functionality to calculate paired country distances based using either the distances between capitals, the distance between the centroid of the nation-states, or the minimum distance between state boundaries. These are excellent tools for several modeling exercises.

As a final note, although GADM has no restrictions for personal and academic use, commercial use requires a licensing fee that is potentially prohibitive for small to medium sized business usage. GAUL and SALB also prohibits commercial use of their data products. This is in stark contrast to Natural Earth and CShapes, which implement Creative Commons licenses, and Esri/Garmin's World Countries data product that is bundled with Esri GIS products and have complex transference rules. 

# References

