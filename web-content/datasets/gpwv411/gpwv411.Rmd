---
metadata-date: "Jun 21, 2021"

author:
- name: Joshua Brinks
  email: jbrinks@isciences.com
  affiliation: ISciences, LLC
- name: Mairead Milan
  email: mmilan@ciesin.columbia.edu
  affiliation: Center for International Earth Science Information Network (CIESIN)

citation-information:
    title: Gridded Population of the World
    edition: Version 4, Revision 11
    publication-date: 2018
    geospatial-data-presentation-form: raster
    publisher: NASA Socioeconomic Data and Applications Center (SEDAC)
    online-linkage: https://sedac.ciesin.columbia.edu/data/collection/gpw-v4
    dante-citekey: CIESIN2018

contact-information:
-
  contact-person: SEDAC User Services
  email-address: ciesin.info@ciesin.columbia.edu
  contact-organization: Center for International Earth Science Information Network

dataset-highlights:
- GPW (v4.11) presents unmodeled global population counts, densities, and several complimentary raster layers at 1km resolution.
- GPW is unmodeled and uses areal-weighting to evenly allocate population across administrative units. This ensures high accuracy to the input (census) data with some notable shortcomings.
- With areal-weighting, the accuracy of a given pixel is a function the input vector geography. This can result in localized accuracy issues in areas with infrequent or inadequate census data.

abstract: "The Gridded Population of the World, Version 4 (GPWv4): Basic Demographic Characteristics, Revision 11 consists of estimates of human population by age and sex as counts (number of persons per pixel) and densities (number of persons per square kilometer), consistent with national censuses and population registers, for the year 2010. To estimate the male and female populations by age in 2010, the proportions of males and females in each 5-year age group from ages 0-4 to ages 85+ for the given census year were calculated. These proportions were then applied to the 2010 estimates of the total population to obtain 2010 estimates of male and female populations by age. In some cases, the spatial resolution of the age and sex proportions was coarser than the resolution of the total population estimates to which they were applied. The population density rasters were created by dividing the population count rasters by the land area raster. The data files were produced as global rasters at 30 arc-second (~1 km at the equator) resolution. To enable faster global processing, and in support of research communities, the 30 arc-second data were aggregated to 2.5 arc-minute, 15 arc-minute, 30 arc-minute and 1 degree resolutions."

use-constraints: "This work is licensed under the Creative Commons Attribution 4.0 International License (http://creativecommons.org/licenses/by/4.0). Users are free to use, copy, distribute, transmit, and adapt the work for commercial and non-commercial purposes, without restriction, as long as clear attribution of the source is provided."

spatial-information:
  bounding-coordinates:
    west-bounding-coordinate: -180.00
    east-bounding-coordinate: 180.00
    north-bounding-coordinate: 90.00
    south-bounding-coordinate: -90.00
  spatial-reference-information:
    coordinate-system: UTM
    resolution: 30
    units: arc-seconds
    geodetic-model: WGS1984
    
time-period-information:
  beginning-date: 2000
  ending-date: 2020
  resolution: 5 years
  
related-packages:
- <a href="/sedacr">sedacr</a>

related-datasets:
- <a href="https://www.worldpop.org/">WorldPop</a>
- <a href="https://landscan.ornl.gov/">LandScan</a>
- <a href="http://sedac.ciesin.columbia.edu/data/collection/grump-v1">GRUMP</a>
- <a href="https://ghslsys.jrc.ec.europa.eu/ghs_pop2019.php">GHS-POP</a>

related-vignettes:
- <a href="/vignettes/exactextractr-pop">Zonal Statistics of Coastal Regions with exactextractr</a>

data-presentation-forms:
- raster
- netcdf
- vector

keywords:
- population
- demographics

browse-image: "gpwv411-browse.jpg"
subhead-image: "gpwv411-subhead.jpg"
image-attribution: Photo by <a href="https://unsplash.com/@yulokchan?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Joseph Chan</a> on <a href="https://unsplash.com/s/photos/population?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
  

bibliography: "gridded-pop.bib"

output: 
  danteSubmit::danteDataset
---

Spatially-explicit raster datasets that accurately depict human population distributions and urban extents are vital components of political science, building climate resilience @Dasgupta2011, disaster management @Ehrlich2018, epidemiology @Dunn2020, conservation biology @Blankespoor2017, and economics research @Rabiee2020. Gridded population datasets are of particular importance in countries or local municipalities that conduct surveys at less frequent intervals or lack infrastructure to collect the data themselves. In order to construct a continuous population surface (raster grid), it's critical to possess high resolution data for human settlements. This presents several challenges, because the most accurate population and census data are collected with tabular surveys at the national and subnational levels. Harmonizing census data across countries while is further complicated by varying standards and procedures between and within countries. Remote sensing techniques have been employed for decades to estimate population distributions. The most common approach use dasymetric mapping that employs some combination of census data, remotely sensed data, aerial imagery, demographic data, economic data, and statistical modeling to create a continuous population grid even in areas with no, or underdeveloped, census data @Eicher2001. Despite these challenges, recent improvements in census data quality, the availability and granularity of complimentary remotely sensed data depicting human settlements, and exponential leaps in available computing power has led to a variety of spatially-explicit gridded population datasets; each with their own strengths and weaknesses @Leyk2019. Some of the current most widely used examples include:

-   [The Gridded Population of the World (GPW)](http://sedac.ciesin.columbia.edu/data/collection/gpw-v4)
-   [Global Human Settlement Layer Population Grid (GHS-POP)](https://ghslsys.jrc.ec.europa.eu/ghs_pop2019.php)
-   [Global Rural Urban Mapping Project (GRUMP)](http://sedac.ciesin.columbia.edu/data/collection/grump-v1)
-   [History Database of the Global Environment Population Grids (HYDE)](http://themasites.pbl.nl/tridion/en/themasites/hyde/download/index-2.html)
-   [LandScan](https://web.ornl.gov/sci/landscan/)
-   [World Population Estimate (WPE)](https://livingatlas.arcgis.com/en/browse/#d=2&q=World%20Population%20Estimate)
-   [WorldPop](https://www.worldpop.org/)

Of these datasets, GPW, GRUMP, GHS-POP, and WorldPop all feature free open-access, 1km or better resolution, multiple snapshots from a time series between 1990-2020, and original and adjusted population estimates. The only exception being that LandScan only presents a single population total and is open access for just research purposes; commercial use requires a licensing fee. ESRI's WPE would also fall in this grouping (2013-2016 time series, 250m resolution, official and adjusted estimates), but it is restricted to ArcGIS license holders. The primary difference between these datasets is in their modeling approach. For a more detailed quick comparison of the most common gridded population data products refer to POPGRID's [Global Population Grids: Summary Characteristics](https://www.popgrid.org/data-docs-table1) and [Table 2](https://essd.copernicus.org/articles/11/1385/2019/essd-11-1385-2019-t02.png) from Leyk et al. 2019.

The Gridded Population of the World collection, version 4.11 (GPW) uses an unmodeled approach to depict population counts and densities on a continuous global raster surface @CIESIN2018a. The core inputs to the model are tabular population data and vectorized administrative boundaries from the 2010 round of Population and Housing Censuses (data collected 2005-2014). The resulting data product is a 1km resolution (at the equator) global population raster layer. Input geographic and population data for GPW are collected at the highest spatial resolution available and integrated using an areal-weighting approach @Sadahiro2000. Population counts are uniformly allocated to grid cells within the unit based on the proportion of the grid cell that falls in that administrative unit @CIESIN2018a. As a result, population count per pixel is a function of the total land area within that administrative unit. The resulting dataset is a readily interpretable geospatial model of global census data, which can be flexibly integrated with social, economic, geo, and remote-sensing datasets @Doxsey-Whitfield2015. In fact, other datasets outlined in this document use GPW as an input to their statistical model.

```{r echo=FALSE, fig.cap="Gridded Population of the World (version 4.11) 2015 sample data.", message=FALSE, warning=FALSE}
library(raster)
library(RColorBrewer)
library(sp)
GPWv411_PopCounts2015<-raster::raster("gpw_v4_population_count_rev11_2010_30_sec.tif")
National_ID<-raster::raster("gpw_v4_national_identifier_grid_rev11_30_sec.tif")

PopCountBreaks=c(1,10,50,100,1000,40000)
PopCountCols=RColorBrewer::brewer.pal(n=6, "YlOrRd")
NIDColor<-rgb(0,0,0, alpha=0.1)


sp::plot(GPWv411_PopCounts2015,
     breaks=PopCountBreaks,
     col=PopCountCols,
     main="Gridded Population of the World-2015 Population Counts",
     legend=F)

sp::plot(National_ID, 
     col=NIDColor,
     add=TRUE,
     legend=F)

legend("bottom",
  legend=c("<1","1-10","10-50","50-100","100-1000","1000+"),
  fill=PopCountCols, xpd=TRUE, cex=.75, x.intersp = .3, horiz = TRUE)

```

GPW contains multiple timesteps. Population data collected in the 2010 round is adjusted using subnational or national growth rates to produce population estimates for the years 2000, 2005, 2010, and 2020. In addition to the timeseries layers, GPW offers several other complimentary raster layers in the collection. These include, a set of estimates adjusted to national population counts from the United Nations World Population Prospects [@CIESIN2018f; @UnitedNations2019], Basic Demographic Characteristics @CIESIN2018c, Data Quality Indicators @CIESIN2018b, Land and Water Areas @CIESIN2018d, and national level numeric identifiers @CIESIN2018e.

GRUMP [@Balk2006; @CIESIN2011] expands on GPW (both datasets are developed, in-part, by CIESIN) with a specific focus on discriminating between urban and rural areas. The population counts, population density, and urban extent raster grids are lightly modeled using dasymetric techniques with the *original* nighttime lights @Elvidge1997 dataset as a covariate. GRUMP contains raster layers for the years 1990, 1995, and 2000, but is not expected to be updated further, because modern nighttime lights data @Elvidge2017 is more commonly used as a proxy for economic development and not urban extents @Leyk2019. For this reason, other datasets may be more appropriate unless you have a specific historic use case.

GHS-POP (also developed, in-part, by CIESIN) is a lightly modeled dataset with population counts at a 250m resolution for the years 1975, 1990, 2000, and 2015. GHS-POP uses GPW's population estimates in conjunction with the Global Human Settlement data (GHS-BUILT @Corbane2018) in a dyasmetric model to allocate census data to individual cells. In some ways, GHS-POP appears to be the natural successor to GRUMP by implementing urban settlement constraints with GHS-BUILT as opposed to nighttime lights data. GHS-POP demonstrates full transparency in their methods and input datasets; their goal is to be fully reproducible with applied uses and support for a variety stakeholders and policy makers.

Oak Ridge National Laboratory's LandScan Global population dataset @Rose2020 is a highly modeled population data product. LandScan employs a multivariate dasymetric modeling framework that uses census data, land cover classes, slope, elevation, roads, urban boundaries, vectorized coastlines, and remotely sensed imagery to disaggregate national and subnational census data to grid cells. LandScan receives regular updates and is free for research or educational purposes, however, they do not make their input data sources available and there is a fee for commercial uses.

Lastly, the WorldPop @WorldPop2021 program offers a multitude of gridded population and demographic raster datasets. These include population counts and densities, gender and age structures, birth and pregnancy rates, development indicators, migration flows, urban changes, and several more. Similar to LandScan, WorldPop is a highly modeled dasymetric dataset that uses the randomForest @Liaw2002 algorithm to allocate census counts and demographics breakdowns to grid cells at a finer resolution than competing products (100m). In contrast to some data products, WorldPop promotes full transparency in their methods and input data sources. Further, they have a geographic focus on Central and South America, Africa, and Asia with a core mission to assist with data availability and policy planning in regions that need it most.

Unmodeled datasets, like GPW, only use census data for areal-weighting and distribution of population to grid cells. The main benefits of this approach are the reduced computational costs and maintaining the highest fidelity to the input data without introducing error from additional datasets and modeling. However, areal-weighted pixel-level estimates have variable precision as a result of differing quality between census data. To assist users in making determinations on data suitability, some datasets include ancillary data layers indicating the frequency, count, and accuracy for each region @CIESIN2018b. This allows users to see where the input census data may lack the necessary fidelity to perform fine-scaled analysis in their region of interest. These issues can manifest themselves in several ways: 1) In nations where population counts are provided at larger geographic units, rural areas may exhibit inflated estimates, 2) along coastlines where the land area in a given pixel may be very small, artificially high population densities can occur next to highly populated areas, and 3) where a census was delayed or did not occur, older or alternative census data may be used instead.

This is not to say that modeled population datasets are without their own peculiarities and levels of uncertainty. While a modeled population estimate (in theory) increases accuracy in regions with inadequate or infrequent census data, they introduce error like any other statistical process. The accuracy and precision of the modeled estimates are a function of the quality of the input data and the strength of the relationship between the census counts and the covariates. Dasymetric covariates with fundamental collection issues, inherent biases, poor statistical relationships with the response metric, or high spatial variability may leave the user no better off than using raw census data. Issues with modeled datasets are further complicated when there is a lack of transparency regarding data sources, pre-processing, and the statistical approach. Lastly, a scarcity of validation datasets for gridded population data products reduces the ability to detect and correct for biases. This affects modeled and unmodeled datasets.

Ultimately it's the user's responsibility to possess a deep understanding of the strengths and weaknesses of the available data products in relation to the spatial scope, scale, and objectives of their analysis.

# Reference
