# danteSubmit 0.1.0

* Added a `NEWS.md` file to track changes to the package.

* Added `README.RMD` and `README.MD` to act as welcome page for GitLab site.

* Refined vignette stylesheet and html template.

* Developed package logo.

* Altered package structure to mirror popular template packages. Template support files buried in ~/resources.

* Established gitlab CI pipeline for remote testing and generation of reference and vignette materials.

* Developed and embedded *Submitting a Dataset to DANTE* vignette.

* Developed and embedded *Submitting a Vignette to DANTE* vignette.
